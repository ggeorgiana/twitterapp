import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class TwitterUser(models.Model):
    screen_name = models.CharField(max_length=160, primary_key=True)

    def __str__(self):
        return self.screen_name


class Tweet(models.Model):
    screen_name = models.ForeignKey(
        TwitterUser,
        to_field="screen_name",
        db_column="screen_name",
        on_delete=models.CASCADE,
    )
    text = models.CharField(max_length=160)
    created_at = models.DateTimeField(auto_now_add=True)
    favorites_count = models.IntegerField()
    retweets_count = models.IntegerField()
    followers_count = models.IntegerField()

    def __str__(self):
        return (
            "\nUser: "
            + str(self.screen_name)
            + "\nText: "
            + self.text
            + "\nCreated date: "
            + str(self.created_at)
        )

    def compute_engagement_score(self):
        engagement_score = (
            self.favorites_count + self.retweets_count
        ) / self.followers_count
        return round(engagement_score, 6)
