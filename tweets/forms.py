from django.forms import ModelForm

from tweets.models import TwitterUser


class TweetForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TweetForm, self).__init__(*args, **kwargs)
        self.fields["screen_name"].required = False

    class Meta:
        model = TwitterUser
        fields = ["screen_name"]
