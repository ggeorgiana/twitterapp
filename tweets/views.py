import base64
from datetime import datetime

import requests
from django.http import HttpResponse
from django.shortcuts import redirect, render

from tweets.forms import TweetForm
from tweets.models import Tweet, TwitterUser

USER_TIMELINE_ENDPOINT = "https://api.twitter.com/1.1/statuses/user_timeline.json"

AUTH_URL = "https://api.twitter.com/oauth2/token"

AUTH_CONSUMER_KEY = "gmhdce3ZOpW1x0Gch6UBm5Ndp"
AUTH_CONSUMER_SECRET = "DmthzkZnIAJVXzcGW0bubzazyy7HXmymLT4BN9YkdpX9b5BzNy"


def index(request):
    return HttpResponse("Hello! Continue to the Twitter App: http://localhost:8000/twitter_app/tweets/")


def from_str_to_date(api_date: str):
    # api_date = "Thu Aug 01 17:26:10 +0000 2019"
    datetime_object = datetime.strptime(api_date, "%a %b %d %H:%M:%S %z %Y")
    return datetime_object


# Twitter Auth API call
def gen_auth_token():
    bearer_token = AUTH_CONSUMER_KEY + ":" + AUTH_CONSUMER_SECRET
    base64_encoded_bearer_token = base64.b64encode(bearer_token.encode("utf-8"))

    headers = {
        "Authorization": f"Basic {base64_encoded_bearer_token.decode('utf-8')}",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        "Content-Length": "29",
    }
    data = {"grant_type": "client_credentials"}

    response = requests.post(url=AUTH_URL, data=data, headers=headers)

    return response.json().get("access_token")


# Twitter API call
def collect_tweets(screen_name, tweets_count=25):
    access_token = gen_auth_token()
    url = f"{USER_TIMELINE_ENDPOINT}?screen_name={screen_name}&count={tweets_count}"
    response = requests.get(
        url=url, headers={"Authorization": f"Bearer {access_token}"}
    )
    json_response = response.json()

    # provided screen_name does not exists
    if response.status_code != 200:
        return [], f'Status code: {response.status_code},  {json_response.get("errors")[0].get("message")}'
        # return [], f"Status code: {response.status_code}, {response.text}"

    error = ""
    tweets = []
    for tweet in json_response:
        user_object = TwitterUser(screen_name=tweet.get("user").get("screen_name"))
        tweet_object = Tweet(
            screen_name=user_object,
            text=tweet.get("text"),
            created_at=from_str_to_date(tweet.get("created_at")),
            favorites_count=tweet.get("favorite_count"),
            retweets_count=tweet.get("retweet_count"),
            followers_count=tweet.get("user").get("followers_count"),
        )
        tweets.append(tweet_object)
    return tweets, error


def get_last_tweets(request):
    tweets = []
    error = ""

    # search button is pressed
    if request.method == "POST":
        form = TweetForm(request.POST)
        if form.is_valid():
            form_input = form.save(commit=False)
            print("FORM input1: ", form_input.screen_name)
            return redirect("save_tweets/" + form_input.screen_name)
    else:
        form = TweetForm()

    return render(
        request, "collect_tweets.html", {"tweets": tweets, "form": form, "error": error}
    )


def save_tweets(request, screen_name):
    tweets, error = collect_tweets(screen_name)
    if request.method == "POST":
        form = TweetForm(request.POST)
        if request.POST.get("save"):
            print("User clicked SAVE btn")
            # print("FORM input2: ", form_input.user)
            save_to_db(tweets)
        if request.POST.get("search"):
            if form.is_valid():
                form_input = form.save(commit=False)
                print("FORM input1: ", form_input.screen_name)
                return redirect("/twitter_app/tweets/save_tweets/" + form_input.screen_name)
    else:
        form = TweetForm()
    return render(
        request, "save_tweets.html", {"tweets": tweets, "form": form, "error": error}
    )


def list_saved_tweets(request):
    db_users = get_users()
    db_tweets = get_tweets()
    if request.method == "POST":
        form = TweetForm(request.POST)
        if request.POST.get("delete"):
            print("User clicked DELETE btn")
            delete_tweets_and_users()
            return redirect("/twitter_app/tweets/list_saved_tweets/")
    else:
        form = TweetForm()
    return render(
        request,
        "list_users.html",
        {"users": db_users, "tweets": db_tweets, "form": form},
    )


def get_users():
    all_users = TwitterUser.objects.all()
    return all_users


def get_tweets():
    all_tweets = Tweet.objects.order_by("-favorites_count", "-retweets_count").all()
    return all_tweets


def delete_tweets_and_users():
    Tweet.objects.all().delete()
    TwitterUser.objects.all().delete()


def save_to_db(tweets: list):
    for tweet in tweets:
        tweet.screen_name.save()
        tweet.save()
    print("Successfully saved: ", tweets)
