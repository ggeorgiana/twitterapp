from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('tweets/', views.get_last_tweets, name='Tweet'),
    path('tweets/save_tweets/<str:screen_name>', views.save_tweets, name='Save Tweet'),
    path('tweets/list_saved_tweets/', views.list_saved_tweets, name="List saved tweets"),
]