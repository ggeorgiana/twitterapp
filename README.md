# itw16
System requirements:

1. Python 3.6.8
2. PostgresSQL 10.9 

Steps to install the project:
1. Clone the project
2. Instal systen requirements
3. Create a Virtual Environment:
```
virtualenv djangovenv
```
4. Activate the venv
```
source djangovenv/bin/activate
``` 
5. Install `requirements.txt`

6. Run migrations:
```
python manage.py makemigrations tweets
python manage.py migrate

```
7. Run the twitter_app:
```
python manage.py runserver
```
8. Open browser: http://localhost:8000/twitter_app/tweets
